# README #

Entrega 2 de Sistemas Op. y redes.
Breve explicacion:

- Codigo salida:
Para esto simplemente utilizamos printf luego de que un proceso llamara a exit(exitcode). --- en syscall.c

- Paso de parametros:
Primero parseamos el nombre y luego modificamos Setup_stack agregando los parametros segun el formato que indica la documentación oficial de Pintos --- en process.c

- Llamadas a sistema: Modificamos el syscall handler y agregamos las siguientes funciones:

* Halt: Llama a la funcion shutdown_power_off
* Exit: Imprime en pantalla el codigo de salida y finaliza el proceso
* Exec: Ejecuta el nuevo proceso y retorna su codigo
* Wait: No implementada
* get_arg: Almacena los parametros deseados en un arreglo (creado en syscall_handler)


PD: El profesor autorizó que fuesemos 3 alumnos por esta vez. El tercer integrante es Fernando Araya.