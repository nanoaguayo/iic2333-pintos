#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "devices/shutdown.h"
#include <string.h>
#include "userprog/process.h"


// Implementation Parameters
// =========================
static int WRITE_SIZE=128;
typedef int pid_t;

void syscall_halt(void);
void syscall_exit(int x);
void get_arg (struct intr_frame *f, int *arg, int n);
pid_t syscall_exec(const char *cmd_line);


inline
int
syscall_write(struct intr_frame *f)
{
  int *p = f->esp;  // Stack pointer (x86 registers http://www.scs.stanford.edu/05au-cs240c/lab/i386/s02_03.htm)

  //FIXME: This is (probably) displaced because of the -12 on process.c:442 (setup_stack)
  //int file_descriptor =         p[1];
  //char        *buffer = (char*) p[2];
  //int            size = (int)   p[3];  // huge size may cause overflow

  int file_descriptor =         p[5];
  char        *buffer = (char*) p[6];
  int            size = (int)   p[7];  // may overflow signed int!!

  switch(file_descriptor)
  {
    case STDIN_FILENO:
      // Inform that no data was written
      f->eax=0;
      // REVIEW: Should that process be terminated?
      return 2;

    case STDOUT_FILENO:
    {
      // Write in chunks of WRITE_SIZE
      //   putbuf (src/lib/kernel/console.c) locks the console,
      //   we should avoid locking it too often or for too long
      int remaining = size;
      while(remaining > WRITE_SIZE)
      {
        // Write a chunk
        putbuf(buffer, WRITE_SIZE);
        // Advance buffer pointer
        buffer    += WRITE_SIZE;
        remaining -= WRITE_SIZE;
      }
      // Write all the remaining data
      putbuf(buffer, remaining);

      // Inform the amount of data written
      f->eax=(int)size;
      return 0;
    }

    default:
      printf("syscall: write call not implemented for files\n");
      return 1;
  }

  return 1;  // Unreachable, but compiler complains
}

void syscall_halt(void){
    
    
    shutdown_power_off();
    
    
    
}


void syscall_exit(int x){
    
    
    printf("%d: exit(%d)\n",thread_current()->tid,x);
    thread_exit();
    
}

pid_t syscall_exec(const char *cmd_line){
    pid_t pid= process_execute(cmd_line);
    
    
    return pid;
}



static void
syscall_handler (struct intr_frame *f)
{
  // intr_frame holds CPU register data
  //   Intel 80386 Reference Programmer's Manual (TL;DR)
  //     http://www.scs.stanford.edu/05au-cs240c/lab/i386/toc.htm
  
  int *p = f->esp;  // Stack pointer (x86 registers http://www.scs.stanford.edu/05au-cs240c/lab/i386/s02_03.htm)
  int syscall_number = (*p);
  
  int args[4];
  switch(syscall_number)
  {
    case SYS_HALT:
      printf("system call: halt\n");
      syscall_halt();
      break;

    case SYS_EXIT:
      printf("system call: exit\n");
      get_arg(f,&args[0],1);
      syscall_exit(args[0]);
      break;

    case SYS_EXEC:
      printf("system call: exec\n");
      get_arg(f, &args[0], 1);
      f->eax = syscall_exec((const char *) args[0]); 
      break;

    case SYS_WAIT:
      printf("system call: wait\n");
      get_arg(f,&args[0],1);
      break;

    case SYS_WRITE:
      syscall_write(f);
      return;

    default:
      printf("system call: unhandled syscall. Terminating process[%d]\n",
             thread_current()->tid);
      break;
  }

  // Syscall handling failed, terminate the process
  thread_exit ();
}

void get_arg (struct intr_frame *f, int *arg, int n)
{
    int i;
    int *ptr;
    for (i = 0; i < n; i++)
    {
        ptr = (int *) f->esp + i + 1;
      
        arg[i] = *ptr;
    }
}

void
syscall_init (void)
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
}

